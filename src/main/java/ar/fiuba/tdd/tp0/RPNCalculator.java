package ar.fiuba.tdd.tp0;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Optional;
import java.util.function.BinaryOperator;

/**
 * Copyright 2015
 * Bruno Merlo Schurmann bruno290@gmail.com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses
 */

/**
 * Reverse Polish Notation Calculator
 */
public class RPNCalculator {

    private Hashtable<String, Operable> operations;

    /**
     * Constructs a new Calculator with the default operations
     */
    public RPNCalculator() {
        this.operations = new Hashtable<>();
        addDefaultBinaryOperators();
        addDefaultNaryOperators();
    }

    /**
     * Registers the default binary operations to the calculator
     */
    private void addDefaultBinaryOperators() {
        registerBinaryOperator("+", (Float augend, Float addend) -> augend + addend);
        registerBinaryOperator("*", (Float multiplicand, Float multiplier) -> multiplicand * multiplier);
        registerBinaryOperator("-", (Float minuend, Float substrahend) -> minuend - substrahend);
        registerBinaryOperator("/", (Float dividend, Float divisor) -> dividend / divisor);
        registerBinaryOperator("MOD", (Float dividend, Float divisor) -> dividend % divisor);
    }

    /**
     * Registers the default n-ary operators to the calculator
     */
    private void addDefaultNaryOperators() {
        registerNaryOperator("++", (Float firstOperator, Float secondOperator) -> firstOperator + secondOperator);
        registerNaryOperator("--", (Float firstOperator, Float secondOperator) -> firstOperator - secondOperator);
        registerNaryOperator("**", (Float firstOperator, Float secondOperator) -> firstOperator * secondOperator);
        registerNaryOperator("//", (Float firstOperator, Float secondOperator) -> firstOperator / secondOperator);
    }

    /**
     * Registers a new binary operator to the calculator by ID
     * @param id string that contains the operator's ID
     * @param function function that receives two floats and returns a float as result of applying it
     */
    public void registerBinaryOperator(String id, BinaryOperator<Float> function) {
        operations.put(id, new BinaryOperation(function));
    }

    /**
     * Registers a new n-ary operator to the calculator by ID
     * @param id string that contains the operator's ID
     * @param function function that receives two floats and returns a float as result of applying it
     */
    public void registerNaryOperator(String id, BinaryOperator<Float> function) {
        operations.put(id, new NaryOperation(function));
    }

    /**
     * Evaluates the expression, calculates the result and returns the result as a float
     * @param expression string that contains the expression to evaluate
     * @return the result of the given expression as a float
     * @throws IllegalArgumentException if the expression is invalid (null or empty, with unknown operators or with
     *         missing numbers
     */
    public float eval(String expression) {
        checkValidExpressionFormat(expression);
        String[] symbols = Parser.getSymbols(expression);
        LinkedList<Float> numbersList = new LinkedList<>();
        int index = 0;
        while (index < symbols.length) {
            String symbol = symbols[index];
            while (Parser.isNumber(symbol)) {
                numbersList.add(Float.parseFloat(symbol));
                symbol = symbols[++index];
            }
            Optional<Operable> operation = Optional.ofNullable(operations.get(symbol));
            operation.orElseThrow(IllegalArgumentException::new);
            float result = operation.get().operate(numbersList);
            System.out.println(result);
            numbersList.add(result);
            index++;
        }
        return numbersList.removeLast();
    }

    /**
     * Checks if the expression has a valid form: it isn't null or empty, and has at least one number. Throws an
     * exception if the expression is invalid.
     * @param expression string that that contains the expression to be evaluated by the calculator
     * @throws IllegalArgumentException if the expression isn't valid
     */
    private void checkValidExpressionFormat(String expression) {
        // Checks for expression == null
        Optional<String> exp = Optional.ofNullable(expression);
        exp.orElseThrow(IllegalArgumentException::new);
        // Checks for a number in the expression
        String[] symbols = Parser.getSymbols(expression);
        Arrays.stream(symbols).filter(Parser::isNumber).findAny().orElseThrow(IllegalArgumentException::new);
    }

}
