package ar.fiuba.tdd.tp0;

import java.util.LinkedList;

/**
 * Copyright 2015
 * Bruno Merlo Schurmann bruno290@gmail.com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses
 */

/**
 * Interface for the calculator operations. Represents an operation that takes a LinkedList of floats, operates with
 * them and returns a float result.
 */
public interface Operable {

    /**
     * Resolves the operation and returns it result
     * @param operands LinkedList of floats that contains the numbers to operate with
     * @return result of the operation, as float
     */
    float operate(LinkedList<Float> operands);
}
