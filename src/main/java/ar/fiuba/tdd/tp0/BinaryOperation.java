package ar.fiuba.tdd.tp0;

import java.util.LinkedList;
import java.util.Optional;
import java.util.function.BinaryOperator;

/**
 * Copyright 2015
 * Bruno Merlo Schurmann bruno290@gmail.com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses
 */

/**
 * Calculator binary operations. Represents an operation that takes a LinkedList of floats with, at  least, two
 * numbers, operates with them and returns a float result.
 */
public class BinaryOperation implements Operable {

    private final BinaryOperator<Float> function;

    /**
     * Constructs a new binary operation that operates using the given function.
     * @param function function that receives two floats and returns a float as result of applying it
     */
    public BinaryOperation(BinaryOperator<Float> function) {
        this.function = function;
    }

    /**
     * Operates with the given numbers, applying the operation.
     * @param operands LinkedList of floats that contains the numbers (at least two) to operate with
     * @return the result of the operation as a float
     * @throws IllegalArgumentException if there are less than two valid numbers to operate with
     */
    @Override
    public float operate(LinkedList<Float> operands) {
        float n2 = getNextNumber(operands);
        float n1 = getNextNumber(operands);
        return function.apply(n1, n2);
    }

    /**
     *
     * @param numbersList LinkedList of floats that contains the numbers used to operate
     * @return the next operand in the list
     * @throws IllegalArgumentException if there are no more numbers in the list
     */
    private float getNextNumber(LinkedList<Float> numbersList) {
        Optional<Float> number = Optional.ofNullable(numbersList.peekLast());
        number.orElseThrow(IllegalArgumentException::new);
        numbersList.removeLast();
        return number.get();
    }

}
