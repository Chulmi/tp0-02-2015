package ar.fiuba.tdd.tp0;

/**
 * Copyright 2015
 * Bruno Merlo Schurmann bruno290@gmail.com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses
 */

/**
 * Parser for the expression to be evaluated by the calculator
 */
public class Parser {

    /**
     * Evaluates if the given symbol is a number
     * @param symbol symbol to evaluate
     * @return boolean indicating if the symbol is a number
     */
    public static boolean isNumber(String symbol) {
        return symbol.matches("-?\\d+(\\.\\d+)?");
    }

    /**
     * Splits the expression into its symbols
     * @param expression expression to evaluate by the calculator
     * @return array of strings with the symbols of the operation
     */
    public static String[] getSymbols(String expression) {
        return expression.split(" ");
    }

}
